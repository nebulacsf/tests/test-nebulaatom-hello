apt-get update && apt-get install -y make binutils g++ git cmake libssl-dev libpoco-dev libmariadb-dev libyaml-cpp-dev wget

wget https://gitlab.com/api/v4/projects/33299473/packages/generic/nebula-atom/v0.10.4/libnebulaatom-0.10.4-Linux.deb

dpkg -i ./libnebulaatom-*-Linux.deb

apt-get -f install

g++ -o ./main ./main.cpp -I/usr/include/nebula-atom/ -I/usr/include/mariadb/ \
-L/usr/lib \
-lnebulaatom \
-lPocoFoundation \
-lPocoNet \
-lPocoNetSSL \
-lPocoUtil \
-lPocoData \
-lPocoDataMySQL \
-lPocoJSON \
-lyaml-cpp
